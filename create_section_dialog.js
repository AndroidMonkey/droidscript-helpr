
function SectionDialogShow() {
  dialog = app.CreateDialog("New Section")
  const background = app.CreateLayout("Linear")
  background.SetSize(0.5, 0.12)
  const spinner = app.CreateSpinner("Example,Markdown,Text")
  const button = app.CreateButton("Create")
  button.SetOnTouch(createSection)
  button.GetDocType = spinner.GetText
  background.AddChild(spinner)
  background.AddChild(button)
  dialog.AddLayout(background)
  dialog.Show()
}

function createSection() {
  const type = this.GetDocType()
  switch (type) {
    case "Text":
      doc.AddText()
      break;
    case "Example":
      doc.AddExample()
      break
    case "Markdown":
      doc.AddMarkdown()
  }
  dialog.Hide()
}

function FileDialogShow() {
  dialog = app.CreateDialog()
}