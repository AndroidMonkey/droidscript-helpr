function createInterface() {
  const layout = app.CreateLayout("Linear", "VCenter,FillXY")
  const scroller = app.CreateScroller(0.94, 0.88)
  const buttonbg = app.CreateLayout("Linear", "Horizontal")
  const menu = CreateButton("bars")
  const open = CreateButton("folder-open")
  const create = CreateButton("plus")
  const save = CreateButton("save")

  documentbg = app.CreateLayout("Linear", -1, -1, "FillXY")
  title = app.CreateTextEdit("untitled")

  create.SetOnTouch(SectionDialogShow)
  layout.SetBackColor("#222299")
  menu.SetOnTouch(showMenu)
  open.SetOnTouch(openDocument)
  save.SetOnTouch(saveDocument)
  scroller.SetBackColor("#222222")
  title.SetTextSize(24)

  buttonbg.AddChild(menu)
  buttonbg.AddChild(open)
  buttonbg.AddChild(create)
  buttonbg.AddChild(save)
  layout.AddChild(title)
  layout.AddChild(scroller)
  layout.AddChild(buttonbg)
  scroller.AddChild(documentbg)

  //Add layout to app.
  app.AddLayout(layout)


  //  Menus don't do anything yet.
  const menus = "Files:Files.png,Settings:Settings.png"
  app.SetMenu(menus, "Img")

}

function CreateButton(text) {
  return app.CreateButton(
    "[fa-"+text+"]", -1, -1, "Gray,FontAwesome")
}


function showMenu() {
  app.ShowMenu()
}

function OnMenu(item) {
  app.ShowPopup(item, "Short")
}

function saveDocument() {
  const path = ddir+"/"+title.GetText()
  app.WriteFile(path+".json", doc)
  app.WriteFile(path+".html", renderDocument(doc))
  app.ShowPopup("Saved", "short")
}

function openDocument() {
  app.ChooseFile("Open which?", "*/*", onFileOpen)
}

function onFileOpen(fileName) {
  var p;
  const f = app.ReadFile(fileName)
  try {
    doc.Clear()
    p = JSON.parse(f)
  }
  catch (e) {
    app.Alert(e)
    throw e
  }
  p.forEach(loadSection)
  //alert(fileName)
}

function loadSection(section) {
  if (section.type === "text") {
    doc.AddText(section.text)
  }
  else if (section.type === "example") {
    doc.AddExample(section.text, section.title)
  }
  else if (section.type === "markdown") {
    doc.AddMarkdown(section.text)
  }
  else {
    throw new error("unknown type")
  }
}