(function (root) {
  /**
   * A container for the text, markdown
   * and example sections.
   *
   * @param {Layout} layout - The layout
   * to afix the sections to.
   */
  function HelpDocument(layout) {
    this.layout = layout
    this.sections = {}
  }

  /**
   * @returns The JSON value of the help
   * document contents.
   */
  HelpDocument.prototype.toString = function () {
    return "[" + Object
      .values(this.sections)
      .join(",\n") + "]"
  }

  /**
   * Adds section to the layout and section
   * list.
   *
   * @param {HelpSection} section
   */
  HelpDocument.prototype.AddSection = function  (section) {
    this.layout.AddChild(section.GetLayout())
    this.sections[section.GetLayout().id] = section
  }

  /**
   * Creates new TextSection and adds it
   * to the document.
   *
   * @param {String} text
   */
  HelpDocument.prototype.AddText = function (text) {
    const section = new TextSection(text)
    this.AddSection(section)
  }

  /**
   * Creates new MarkdownSection and adds it
   * to the document.
   *
   * @param {String} text
   */
  HelpDocument.prototype.AddMarkdown = function (text) {
    const section = new MarkdownSection(text)
    this.AddSection(section)
section
  }

  /**
   * Creates new ExampleSection and adds it
   * to the document.
   *
   * @param {String} text
   * @param {String} title
   */
  HelpDocument.prototype.AddExample = function (text, title) {
    const section = new ExampleSection(text, title)
    this.AddSection(section)
section
  }

  /**
   * Removes section from the list of
   * sections and the layout.
   *
   * @param {HelpSection} section
   */
  HelpDocument.prototype.RemoveSection = function (section) {
    delete this.sections[section.id]
    this.layout.DestroyChild(section)
  }

  /**
   * Removes all sections from the list of
   * sections and the layout.
   */
  HelpDocument.prototype.Clear = function () {
    for (let id in this.sections) {
      let section = this.sections[id]
      this.layout.DestroyChild( section.GetLayout())
      delete this.sections[id]
    }
  }

  /**
   * Create help document.
   *
   * @param {Layout} layout
   * @returns HelpDocument
   */
  root.CreateHelpDocument = function (layout) {
    return new HelpDocument(layout)
  }

  /**
   * A section of the help document,
   * subclass this for specific section
   * types.
   *
   * @param {String} text - Text to set section to.
   */
  function HelpSection(text) {
    this.text = app.CreateTextEdit(text, 0.94, -1)
    this.background = app.CreateLayout("Linear")
    this.header = app.CreateLayout("Linear", "Horizontal")
    this.toggle = CreateButton("angle-up")
    this.erase = CreateButton("trash-o")

    this.toggle.SetOnTouch(toggleSection)
    this.toggle.text = this.text

    this.erase.SetOnTouch(eraseSection)
    this.erase.section = this.background
    this.erase.sectionId = this.background.id

    this.header.AddChild(this.toggle)
    this.header.AddChild(this.erase)
    this.background.AddChild(this.header)
    this.background.AddChild(this.text)

    this.GetLayout = function () {
      return this.background
    }
    this.toggle.Collapse = function () {
      this.text.SetVisibility("Gone")
      this.SetText("[fa-angle-down]")
    }
    this.toggle.Expand = function () {
      this.text.SetVisibility("Show")
      this.SetText("[fa-angle-up]")
    }
  }

  /**
   * Section of plain text,
   * subclass of HelpSection.
   *
   * @param {string} [text="Add text here."]
   */
  function TextSection(text = "Add text here.") {
    HelpSection.call(this, text)
    this.type = "text"
    this.header.SetBackColor("#884444")
    this.spacer = app.CreateText("", 0.74, 0.01)
    this.header.AddChild(this.spacer, 1)
    this.toString = () => `{"type":"text","text": ${JSON.stringify(this.text.GetText())}}`
  }

  /**
   * Section of Markdown formatted text,
   * subclass of HelpSection.
   *
   * @param {string} [text="Add markdown here."]
   */
  function MarkdownSection(text = "Add markdown here.") {
    HelpSection.call(this, text)
    this.type = "markdown"
    this.header.SetBackColor("#448844")
    this.spacer = app.CreateText("", 0.74, 0.01)
    this.header.AddChild(this.spacer, 1)
    this.toString = function () {
      return `{"type":"markdown","text": ${JSON.stringify(this.text.GetText())}}`
    }
  }

  /**
   * Section of plain text, converted
   * into example formatted html,
   * subclass of HelpSection.
   *
   * @param {string} [text="Add example code here."]
   * @param {string} [title="Example Title"]
   */
  function ExampleSection(text = "Add example code here.", title = "Example Title") {
    HelpSection.call(this, text)
    this.type = "example"
    this.header.SetBackColor("#444488")
    this.title = app.CreateTextEdit(title, 0.74, 0.06, "SingleLine")
    this.header.AddChild(this.title, 1)
    this.toString = function () {
      return `{"type":"example", "title":${JSON.stringify(this.title.GetText())}, "text":${JSON.stringify(this.text.GetText())}}`
    }
  }

  /**
   * A shortcut function to `app.CreateButton`
   * so all the buttons will look the same.
   *
   * @param {String} text - The button's label.
   * @returns Button
   */
  function CreateButton(text) {
    return app.CreateButton("[fa-" + text + "]", 0.1, 0.06, "Gray,FontAwesome")
  }

}(this))

/**
 * Toggles the visibility of a section by
 * calling the proper method of the section.
 */
function toggleSection() {
  if (this.GetText() === "\uf106") {
    this.Collapse()
  } else {
    this.Expand()
  }
}

/**
 * Opens dialog to confirm erasing section.
 */
function eraseSection() {
  cursec = this.section
  const dlg = app.CreateYesNoDialog("Remove Section?")
  dlg.SetOnTouch(eraseSectionConfirmed)
  dlg.Show()
}

/**
 * If erasure confirmation dialog result
 * is "yes", then call document method to
 * erase section.
 *
 * @param {String} result
 */
function eraseSectionConfirmed(result) {
  if (result === "Yes") {
    doc.RemoveSection(cursec)
  }
}