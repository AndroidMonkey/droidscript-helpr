/* renderDocument
 *
 * Creates the html document by filling in
 *   different templates for the different
 *   section types.
 *
 * Note: the HTML entity "&nbsp;" was mostly
 *   eliminated from the templates used
 *   below, instead using CSS to preserve
 *   whitespace, or the JavaScript character
 *   equivalent of \xA0.
 */
(function (root) {
  
  alert(title.GetText())

  const documentTemplatePre = ()=>`
<!DOCTYPE html>
<html>

  <head>
    <title>${title.GetText()}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

  <body>
    <div data-role="page" data-theme="a">

      <div data-role="header" data-position="fixed">
        <a href='#' class='ui-btn-left' data-icon='arrow-l' data-theme="c" onclick="history.back(); return false">Back</a>
	    <h1>${title.GetText()}</h1>
      </div><!-- /header -->

      <div data-role="content">

	    <div align="center"><img src="%title%.png" width="150px"></div>

`
  const documentTemplatePost = ()=>`

      </div><!-- /content -->
    </div><!-- /page -->
  </body>
</html>
`

  function renderDocument(doc) {

    return documentTemplatePre()
      + Object.values(doc.sections)
          .map(renderSection)
          .join("\n")
      + documentTemplatePost()
  }

  let exampleId = 0

  function renderSection(section) {
    if (section.type === "text") {
      return "<p>" +
        section.text.GetText()
        .split("\n")
        .join("</p>\n<p>") +
        "</p>"
    } else if (section.type === "example") {
      exampleId += 1
      let result = `
    		<div data-role="collapsible" data-collapsed="true"  data-mini="true" data-theme="a" data-content-theme="b">
			<h3>${section.title.GetText()}</h3>
			<div id="examp${exampleId}" style="font-size:70%; white-space:pre;">
${section.text.GetText()}
            </div>
			<div name="divCopy" align="right">
			<a href="#" data-role="button" data-mini="true" data-inline="true" onclick="copy(snip${exampleId})">\xA0\xA0\xA0Copy\xA0\xA0\xA0</a>
			<a href="#" data-role="button" data-mini="true" data-inline="true" onclick="copy(examp${exampleId})">Copy All</a>
			<a href="#" data-role="button" data-mini="true" data-inline="true" onclick="demo(examp${exampleId})">\xA0\xA0\xA0Run\xA0\xA0\xA0</a>
			</div>
		</div>`

      return result
    } else if (section.type === "markdown") {
      return sd.MakeHtml(section.text.GetText())
    } else {
      return "<!-- unknown type -->"
    }
  }

  root.renderDocument = renderDocument
}(this))