"use strict"

_AddOptions("NoDom")

const hdir = "/sdcard/helpr"
const ddir = "/sdcard/helpr/documents"

var doc
var documentbg
var dialog
var cursec
var sd
var title

app.LoadPlugin("Showdown")
app.Script("example_code.js")
app.Script("create_interface.js")
app.Script("create_section_dialog.js")
//app.Script("create_document.js")
app.Script("helpdocument.js")

//Called when application is started.
function OnStart() {
  if (!app.IsFolder(hdir)) {
    app.MakeFolder(hdir)
  }
  if (!app.IsFolder(ddir)) {
    app.MakeFolder(ddir)
  }

  createInterface()

  app.Script("render.js")


  sd = app.CreateObject("Showdown")

  doc = CreateHelpDocument(documentbg)
  doc.AddMarkdown("*Markdown Example*")
  doc.AddText("Database Example")
  doc.AddText("Another Longer\n\nsection of text")

  doc.AddExample(example_code, "Database Example")

}
