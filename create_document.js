/*
 * Creates document.
 */
function CreateDocument(layout) {

  this.layout = layout
  this.sections = []

  this.AddText = function (text = "help text") {
    const _text = app.CreateTextEdit(text, 1)
    _text.SetTextSize(14)
    _text.SetBackColor("#cc22cc")
    this.layout.AddChild(_text)
    _text.toString = _text.GetText
    this.sections.push(_text)
  }

  this.AddExample = function (text = "example text", title = "title") {
    const example = app.CreateLayout("Linear", "FillX")
    const header = app.CreateLayout("Linear", "Left,Horizontal,FillX")
    const toggle = app.CreateButton("[fa-angle-up]", 0.1, -1, "FontAwesome")
    const _title = app.CreateTextEdit(title, 0.8, -1, "SingleLine")
    const remove = app.CreateButton("[fa-trash-o]", 0.1, -1, "FontAwesome")
    _title.SetBackColor("#222299")
    header.AddChild(toggle)
    header.AddChild(_title)
    header.AddChild(remove)
    const _text = app.CreateTextEdit(text, 1)
    _text.SetTextSize(10)
    _text.SetBackColor("#2222cc")
    toggle.text = _text
    toggle.Collapse = function ()  {
      _text.SetVisibility("Gone")
      toggle.SetText("[fa-angle-down]")
    }
    toggle.Expand = function () {
      _text.SetVisibility("Show")
      toggle.SetText("[fa-angle-up]")
    }
    toggle.toString = toggle.GetText
    example.AddChild(header)
    example.AddChild(_text)
    toggle.SetOnTouch(toggleSection)
    // function () {}
    example.toString = _text.GetText
    this.layout.AddChild(example)
    this.sections.push(example)

  }
}

/*
function toggleSection() {
  if (this.GetText() === "\uf106") {
    this.SetText("[fa-angle-down]")
    this.text.SetVisibility("Gone")
  } else {
    this.SetText("[fa-angle-up]")
    this.text.SetVisibility("Show")
  }
}
*/

function toggleSection() {
  if (this.GetText() === "\uf106") {
    this.Collapse()
  }
  else {
    this.Expand()
  }
}